import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * https://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/ 참고
 */
public class Main {
	private static final String XML_FILE_PATH = "input_files/data.xml";

	public static void main(String[] args) {
		try {
			Document document = buildDocumentFromXml(XML_FILE_PATH);

			System.out.println("------------------------------------------------------------");

			Element documentElement = document.getDocumentElement();
			System.out.println("documentElement :" + documentElement.getNodeName());

			System.out.println("\n------------------------------first------------------------------");

			NodeList firstChildNodes = documentElement.getChildNodes();
			printNodeList(firstChildNodes);

			System.out.println("\n------------------------------second------------------------------");
			for (int i = 0; i < firstChildNodes.getLength(); i++) {
				Node firstChildNode = firstChildNodes.item(i);
				if (firstChildNode.getNodeType() == Node.ELEMENT_NODE) {
					System.out.println("---------------" + ((Element) firstChildNode).getAttribute("name") + "---------------");
				}
				NodeList secondChildNodes = firstChildNode.getChildNodes();
				printNodeList(secondChildNodes);
			}

			System.out.println("\n-------------------------------third-----------------------------");
			System.out.println("아 더 하기 귀찮당 -_-");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Document buildDocumentFromXml(String xmlFilePath) throws ParserConfigurationException, SAXException, IOException {
		File xmlFile = new File(xmlFilePath);
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		return documentBuilder.parse(xmlFile);
	}

	private static void printNodeList(NodeList nodeList) {
		for (int index = 0; index < nodeList.getLength(); index++) {
			Node node = nodeList.item(index);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				System.out.println(((Element) node).getAttribute("name"));
			}
		}
	}
}
